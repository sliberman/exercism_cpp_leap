namespace leap {
  bool is_leap_year(int year);
  bool is_divisible_by(int number, int denominator);
}
